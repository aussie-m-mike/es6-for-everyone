export function fight(firstFighter, secondFighter) {

  let attacker, defender;

  [attacker, defender] = [{...firstFighter}, {...secondFighter}];

  while (attacker.health > 0) {
    defender.health -= getDamage(attacker, defender);
    [attacker, defender] = [defender, attacker];
  }

  return defender;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance();
}

function criticalHitChance() {
  return Math.random() + 1
}

function dodgeChance() {
  return Math.random() + 1
}