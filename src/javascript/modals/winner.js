import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
  const title = 'The winner is';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const nameElement = createName(name);
  const imgElement = createImage(source);

  fighterDetails.append(nameElement, imgElement);

  return fighterDetails;
}

function createName(name) {
  const nameElement = createElement({ tagName: 'h2', className: 'fighter-name' });
  nameElement.innerText = `Name: ${name}`;

  return nameElement;
}

function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}