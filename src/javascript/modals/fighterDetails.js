import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const nameElement = createName(name);
  const imgElement = createImage(source);
  const fighterDataContainer = createElement({ tagName: 'ul', className: 'fighter-data' });
  const attackElement = createAttack(attack);
  const defenseElement = createDefense(defense);
  const healthElement = createHealth(health);

  fighterDataContainer.append(attackElement, defenseElement, healthElement);

  fighterDetails.append(nameElement, imgElement, fighterDataContainer);

  return fighterDetails;
}

function createName(name) {
  const nameElement = createElement({ tagName: 'h2', className: 'fighter-name' });
  nameElement.innerText = `Name: ${name}`;

  return nameElement;
}

function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}

function createAttack(attack) {
  const attackElement = createElement({ tagName: 'li', className: 'fighter-attack' });
  attackElement.innerText = `Attack: ${attack}`;

  return attackElement;
}

function createDefense(defense) {
  const defenseElement = createElement({ tagName: 'li', className: 'fighter-defense' });
  defenseElement.innerText = `Defense: ${defense}`;

  return defenseElement;
}

function createHealth(health) {
  const healthElement = createElement({ tagName: 'li', className: 'fighter-health' });
  healthElement.innerText = `Health: ${health}`;

  return healthElement;
}